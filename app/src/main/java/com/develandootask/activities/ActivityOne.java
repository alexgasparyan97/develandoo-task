package com.develandootask.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.develandootask.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityOne extends AppCompatActivity {
    @BindView(R.id.et_field_max) EditText etFieldMax;
    @BindView(R.id.et_field_limit) EditText etFieldLimit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);
        ButterKnife.bind(this);

        etFieldLimit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    onNextClicked();
                    return true;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.btn_next)
    public void onNextClicked(){
        String str_field_max = etFieldMax.getText().toString();
        String str_field_limit = etFieldLimit.getText().toString();
        if(str_field_max.isEmpty()){
            etFieldMax.requestFocus();
            Toast.makeText(this, "Add max count", Toast.LENGTH_SHORT).show();
            return;
        }
        if(str_field_limit.isEmpty()){
            etFieldLimit.requestFocus();
            Toast.makeText(this, "Add limit count", Toast.LENGTH_SHORT).show();
            return;
        }

        int field_max = Integer.parseInt(str_field_max);
        int field_limit = Integer.parseInt(str_field_limit);
        if(field_max == 0) {
            etFieldMax.requestFocus();
            Toast.makeText(this, "Max cannot be zero", Toast.LENGTH_SHORT).show();
            return;
        }
        if(field_limit ==0){
            etFieldLimit.requestFocus();
            Toast.makeText(this, "Limit cannot be zero", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(ActivityOne.this, ActivityTwo.class);
        intent.putExtra("data_max", Integer.parseInt(str_field_max));
        intent.putExtra("data_limit", Integer.parseInt(str_field_limit));
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

}

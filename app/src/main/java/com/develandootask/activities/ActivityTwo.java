package com.develandootask.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.develandootask.listview_adapters.CustomAdapter;
import com.develandootask.R;
import com.develandootask.helpers.RowData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ActivityTwo extends AppCompatActivity implements AbsListView.OnScrollListener{
    @BindView(R.id.lv_content_holder) ListView lvContent;
    private List<RowData> rowDataArray;
    private boolean isDataLoading = false;
    private int maxNumber;
    private int limitNumber;
    private final int TIMEOUT_TIME = 3000;
    private CustomAdapter listAdapter;
    private final String DATA_REQUEST_TAG = "data_request_tag";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);
        ButterKnife.bind(this);
        maxNumber = getIntent().getIntExtra("data_max", 0);
        limitNumber = getIntent().getIntExtra("data_limit", 0);
        rowDataArray = new ArrayList<>();
        lvContent.setOnScrollListener(this);
    }

    private void receiveData(){
        if(rowDataArray.size() == maxNumber)
            return;

        isDataLoading = true;
        final String url = "https://randomuser.me/api/?inc=name,picture&results=" + Math.min(maxNumber - rowDataArray.size(), limitNumber);
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    isDataLoading = false;
                    JSONArray dataArray = response.getJSONArray("results");
                    for(int i = 0; i < dataArray.length(); i++)
                        rowDataArray.add(new RowData(dataArray.getJSONObject(i)));

                    if(listAdapter == null) {
                        listAdapter = new CustomAdapter(getBaseContext(), (ArrayList<RowData>) rowDataArray);
                        lvContent.setAdapter(listAdapter);
                    }
                    else
                        listAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                    isDataLoading = false;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getBaseContext(), "Cannot display data", Toast.LENGTH_SHORT).show();
            }
        });
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_TIME, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonRequest.setTag(DATA_REQUEST_TAG);
        queue.add(jsonRequest);
    }


    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        /*Onscroll is triggered for first data to be received as well */
        int lastVisibleItem = firstVisibleItem + visibleItemCount;
        if(lastVisibleItem == totalItemCount && !isDataLoading)
            receiveData();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.cancelAll(DATA_REQUEST_TAG);
        if(listAdapter != null)
            listAdapter.clearImageCache();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }
}

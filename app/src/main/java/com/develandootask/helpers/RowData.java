package com.develandootask.helpers;


import org.json.JSONException;
import org.json.JSONObject;

public class RowData {
    private String text;
    private String imageUrl;


    public RowData(String text, String imageUrl){
        this.text = text;
        this.imageUrl = imageUrl;
    }

    public RowData(JSONObject object) throws JSONException{
        JSONObject nameObject = object.getJSONObject("name");
        this.text = nameObject.getString("first").toUpperCase() + " " + nameObject.getString("last").toUpperCase();
        this.imageUrl = object.getJSONObject("picture").getString("medium");
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}

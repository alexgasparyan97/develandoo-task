package com.develandootask.listview_adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.develandootask.R;
import com.develandootask.helpers.RowData;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CustomAdapter extends BaseAdapter {
    private List<RowData> rowDataArray;
    private Context context;
    private DisplayImageOptions displayImageOptions;

    public CustomAdapter(Context context, ArrayList<RowData> rowDataArray){
        this.context = context;
        this.rowDataArray = rowDataArray;
        this.displayImageOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(false)
                .considerExifParams(true)
                .build();

        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY);
        config.defaultDisplayImageOptions(displayImageOptions);
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        ImageLoader.getInstance().init(config.build());
    }


    public void clearImageCache(){
        ImageLoader.getInstance().clearMemoryCache();
    }

    @Override
    public int getCount() {
        return rowDataArray.size();
    }

    @Override
    public RowData getItem(int position) {
        return rowDataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View rowView, ViewGroup viewGroup) {
        final ViewHolder holder;
        if(rowView != null)
            holder = (ViewHolder) rowView.getTag();
        else{
            rowView = LayoutInflater.from(context).inflate(R.layout.list_item_main, viewGroup, false);
            holder = new ViewHolder(rowView);
            rowView.setTag(holder);
        }
        RowData rowData = getItem(position);
        holder.tvRowText.setText(String.valueOf(position + 1) + ". " + rowData.getText());
        ImageLoader.getInstance().displayImage(rowData.getImageUrl(), holder.ivRowImage, displayImageOptions, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                holder.ivRowImage.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                holder.ivRowImage.setImageResource(R.mipmap.ic_launcher);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.ivRowImage.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
        return rowView;
    }

    public static class ViewHolder{
        @BindView(R.id.tv_row_text) TextView tvRowText;
        @BindView(R.id.iv_row_image) ImageView ivRowImage;

        public ViewHolder(View view){
            ButterKnife.bind(this, view);
        }
    }
}
